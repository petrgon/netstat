#ifndef SOCKET_PROTOCOL_H 
#define SOCKET_PROTOCOL_H
#include <string>
#include <iostream>

enum class SocketProtocol {
    tcp,
    udp,
    tcp6,
    udp6,
    all
};

inline std::ostream& operator<<(std::ostream& os, const SocketProtocol& sp) {
  switch (sp)
  {
    case SocketProtocol::tcp:
      return os << "tcp";
    case SocketProtocol::tcp6:
      return os << "tcp6";
    case SocketProtocol::udp:
      return os << "udp";
    case SocketProtocol::udp6:
      return os << "udp6";
    case SocketProtocol::all:
      return os << "all";
    default:
      return os << "unkn";
  }
}

#endif // SOCKET_PROTOCOL_H