#ifndef IP_CONVERT_H
#define IP_CONVERT_H

#include <string>
#include <sstream>
#include <iostream>

#include <arpa/inet.h>
#include <netinet/in.h>

class IpConvert
{
  public:
    static std::string hextoip(std::string hex);

  private:
    static std::string hextoip4(std::string hex);
    static std::string hextoip6(std::string hex);
    static std::string * split(std::string hex);
};

#endif // IP_CONVERT_H