#ifndef SOCKET_H
#define SOCKET_H
#include <string>
#include <iostream>
#include <iomanip>

#include "Process.h"
#include "SocketProtocol.h"
#include "Filter.h"
#include "Regex.h"
#include "IpConvert.h"

struct Process;

class Socket
{
public:
  friend std::ostream &operator<<(std::ostream &os, const Socket &soc);

  bool match(Filter *filter);

  const std::string getLocAdr() const;
  const std::string getLocAdr();
  const std::string getRemAdr() const;
  const std::string getRemAdr();

  SocketProtocol protocol;
  int inode;
  Process *process;
  std::string localAddress;
  std::string remAddress;

private:
  std::string cached_remAdr;
  std::string cached_locAdr;

};

#endif // SOCKET_H