#ifndef PROCESS_H
#define PROCESS_H
#include <string>
#include <list>

#include "Socket.h"
#include "Filter.h"
#include "Regex.h"

struct Socket;

struct Process
{
    friend std::ostream &operator<<(std::ostream &os, const Process &proc);

    bool match(Filter *filter);

    std::string procesId;
    std::string processCmdline;
    std::list<Socket *> sockets;
};

#endif // PROCESS_H