#ifndef PROCESS_PARSER_H
#define PROCESS_PARSER_H
#include <list>
#include <algorithm>
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "Process.h"
#include "SocketProtocol.h"
#include "DataHolder.h"

class ProcessParser
{
public:
  ProcessParser() = default;

  std::list<Process *> getProcessesWithOpenSocket();

private:
  bool isOnlyNumbers(std::string name);
  Process *resolveProcess(std::string processPath);
  void resolveCmdLine(Process *proc, std::string procPath);
  void resolveSocket(Process *proc, std::string socketPath);

  static const std::string PROCESSES_DIR;
  static const std::string FILE_DESCRIPTOR_DIR;
  static const std::string CMD_LINE;
};

#endif // PROCESS_PARSER_H