#ifndef ARGS_RESOLVER_H
#define ARGS_RESOLVER_H
#include <string>
#include <getopt.h>

#include "Filter.h"
#include "SocketProtocol.h"

class ArgsResolver
{
    public:
    ArgsResolver() = default;
    Filter * resolve(const int & argc, char * const argv[]);
};

#endif // ARGS_RESOLVER_H