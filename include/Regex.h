#ifndef REGEX_H
#define REGEX_H

#include <iostream>
#include <sys/types.h>

#ifndef C_REGEX
#include <regex>
#else
#include <regex.h>
#endif

class Regex
{
  public:
    #ifndef C_REGEX
    static bool compare(const std::string &regex, const std::string &str);
    #else
    static bool compare_c(const char *regex, const char *str);
    #endif
};

#endif // REGEX_H