#ifndef NETSTAT_H
#define NETSTAT_H
#include <list>
#include <algorithm>
#include <iterator>
#include <iomanip>

#include "Filter.h"
#include "ProcNetParser.h"
#include "ProcessParser.h"
#include "DataHolder.h"
#include "SocketProtocol.h"

struct Filter;

class Netstat
{
  public:
    Netstat() = default;
    virtual ~Netstat();
    void printList(Filter *filter);

  private:
    void printUdp(Filter *filter);
    void printTcp(Filter *filter);
    void printDescription();
    void initializeData();
    void initializeProcNet();
    void initializeProcesses();
};

#endif // NETSTAT_H