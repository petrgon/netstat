#ifndef NET_TCP_PARSER_H 
#define NET_TCP_PARSER_H
#include <string>
#include <sstream>
#include <iostream>
#include <fstream>
#include <list>

#include "Socket.h"
#include "SocketProtocol.h"

class ProcNetParser{
    public:
        ProcNetParser() = default;

        std::list<Socket *> parseTcp();
        std::list<Socket *> parseUdp();
        std::list<Socket *> parseTcp6();
        std::list<Socket *> parseUdp6();

    private:
        std::list<Socket *> parse(std::string file, SocketProtocol protocol);

    static const std::string NET_TCP_PATH;
    static const std::string NET_UDP_PATH;
    static const std::string NET_TCP6_PATH;
    static const std::string NET_UDP6_PATH;
};

#endif // NET_TCP_PARSER_H