#ifndef FILTER_H
#define FILTER_H
#include <string>

#include "SocketProtocol.h"

struct Filter
{

    friend std::ostream &operator<<(std::ostream &os, const Filter &filter);

    SocketProtocol protocol = SocketProtocol::all;
    std::string regex;
};

#endif // FILTER_H