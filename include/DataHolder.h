#ifndef DATA_HOLDER_H 
#define DATA_HOLDER_H
#include <list>

#include "Socket.h"
#include "Process.h"

struct Socket;
struct Process;

struct DataHolder {
    static DataHolder * getInstance();
    static void destroy();

    std::list<Socket *> tcpSoc;
    std::list<Socket *> udpSoc;
    std::list<Process *> processes;

    private:
    DataHolder() = default;
    static DataHolder * instance_;
};

#endif // DATA_HOLDER_H