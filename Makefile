CC	= g++

IDIR = include
TDIR = test
ODIR = build
LDIR = lib
SDIR = src

#optional CFLAGS: -DC_REGEX
CFLAGS	= -std=c++17 -g -Wall -pedantic -I$(IDIR)
LDFALGS	=
LIBS =

_DEPS = SocketProtocol.h  
DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))

_OBJ = ProcNetParser.o DataHolder.o ProcessParser.o Netstat.o Socket.o Process.o ArgsResolver.o Filter.o Regex.o IpConvert.o
OBJ = $(ODIR)/main.o $(patsubst %,$(ODIR)/%,$(_OBJ))

TEST_OBJ = $(ODIR)/maintest.o $(patsubst %,$(ODIR)/%,$(_OBJ))

.PHONY: all clean test

all: hw1

$(ODIR)/%.o: $(SDIR)/%.cpp $(DEPS) | $(ODIR)
	$(CC) -c -o $@ $< $(CFLAGS)

$(ODIR):
	mkdir $@

hw1: $(OBJ)
	$(CC) -o $@ $^ $(LDFLAGS) $(LIBS)

$(ODIR)/maintest.o: $(TDIR)/maintest.cpp $(DEPS) | $(ODIR)
	$(CC) -c -o $@ $< $(CFLAGS)

test: $(TEST_OBJ)
	$(CC) -o $@.run $^ $(LDFLAGS) $(LIBS)

clean:
	rm -rf $(ODIR) hw1 test.run
