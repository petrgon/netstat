#include "../include/DataHolder.h"

DataHolder *DataHolder::instance_ = nullptr;

DataHolder *DataHolder::getInstance()
{
    if (!instance_)
        instance_ = new DataHolder();
    return instance_;
}

void DataHolder::destroy()
{
    if (instance_){
        for(Process * process  : instance_->processes){
            delete(process);
        }
        for(Socket * socket  : instance_->udpSoc){
            delete(socket);
        }
        for(Socket * socket  : instance_->tcpSoc){
            delete(socket);
        }
        delete (instance_);
    }
    instance_ = nullptr;
}