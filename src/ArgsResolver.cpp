#include "../include/ArgsResolver.h"

Filter *ArgsResolver::resolve(const int &argc, char *const argv[])
{
    Filter *filter = new Filter();
    int c;
    while (1)
    {
        static struct option long_options[] = {
            {"tcp", no_argument, NULL, 't'},
            {"udp", no_argument, NULL, 'u'},
            {0, 0, 0, 0}};
        c = getopt_long(argc, argv, "tu",
                        long_options, NULL);
        if (c == -1)
            break;
        switch (c)
        {
        case 'u':
            if (filter->protocol != SocketProtocol::all)
                filter->protocol = SocketProtocol::all; //both options set
            else
                filter->protocol = SocketProtocol::udp;
            break;
        case 't':
            if (filter->protocol != SocketProtocol::all)
                filter->protocol = SocketProtocol::all; //both options set
            else
                filter->protocol = SocketProtocol::tcp;
            break;
        default:
            exit(EXIT_FAILURE);
        }
    }
    if (optind < argc)
    {
        while (optind < argc)
            filter->regex.append(argv[optind++]);
    }
    return filter;
}