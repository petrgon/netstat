#include "../include/Regex.h"

#ifndef C_REGEX
bool Regex::compare(const std::string &regex, const std::string &str)
{
    return std::regex_match(str, std::regex(regex));
}
#else
bool Regex::compare_c(const char * regex, const char * str){
    regex_t preg;
    int errcode = regcomp(&preg, regex, 0 ); //possible REG_EXTENDED
    if (errcode == 0)
        errcode = regexec(&preg, str, 0, nullptr, 0);
    if (errcode != 0)
    {
        size_t buffsize = regerror(errcode, &preg, nullptr, 0);
        char *buff = new char[buffsize];
        regerror(errcode, &preg, buff, buffsize);
        std::cerr << buff << std::endl;
        delete[] buff;
        regfree(&preg);
        exit(EXIT_FAILURE);
    }
    regfree(&preg);
    return true;
}
#endif