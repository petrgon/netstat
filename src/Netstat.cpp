#include "../include/Netstat.h"

Netstat::~Netstat()
{
    DataHolder::destroy();
}

void Netstat::printList(Filter *filter)
{
    initializeData();
    if (filter->protocol == SocketProtocol::all ||
        filter->protocol == SocketProtocol::tcp ||
        filter->protocol == SocketProtocol::tcp6)
        printTcp(filter);
    if (filter->protocol == SocketProtocol::all)
        std::cout << "\n";
    if (filter->protocol == SocketProtocol::all ||
        filter->protocol == SocketProtocol::udp ||
        filter->protocol == SocketProtocol::udp6)
        printUdp(filter);
}

void Netstat::initializeData()
{
    // keep order!
    initializeProcNet();
    initializeProcesses();
}

void Netstat::initializeProcNet()
{
    ProcNetParser procnet;
    DataHolder *data = DataHolder::getInstance();
    auto tmp = procnet.parseTcp();
    data->tcpSoc.splice(data->tcpSoc.end(), std::move(tmp));
    tmp = procnet.parseTcp6();
    data->tcpSoc.splice(data->tcpSoc.end(), std::move(tmp));
    tmp = procnet.parseUdp();
    data->udpSoc.splice(data->udpSoc.end(), std::move(tmp));
    tmp = procnet.parseUdp6();
    data->udpSoc.splice(data->udpSoc.end(), std::move(tmp));
}

void Netstat::initializeProcesses()
{
    DataHolder *data = DataHolder::getInstance();
    ProcessParser procParser;
    auto procs = procParser.getProcessesWithOpenSocket();
    data->processes.splice(data->processes.end(), std::move(procs));
}

void Netstat::printDescription()
{
    std::cout << std::left << std::setw(6)  << "Proto"
              << std::setw(24) << "Local Address"
              << std::setw(24) << "Foreign Address"
              << "PID/Program name and arguments\n";
}

void Netstat::printUdp(Filter *filter)
{
    DataHolder *data = DataHolder::getInstance();
    std::cout << "List of UDP connections:\n";
    printDescription();
    for (auto soc : data->udpSoc)
        if (soc->match(filter) || (soc->process && soc->process->match(filter)))
            std::cout << *soc << "\n";
}

void Netstat::printTcp(Filter *filter)
{
    DataHolder *data = DataHolder::getInstance();
    std::cout << "List of TCP connections:\n";
    printDescription();
    for (auto soc : data->tcpSoc)
        if (soc->match(filter) || (soc->process && soc->process->match(filter)))
            std::cout << *soc << "\n";
}
