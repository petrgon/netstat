#include "../include/Filter.h"

std::ostream& operator<<(std::ostream& os, const Filter& filter)
{
    os << filter.protocol << " " << filter.regex;
    return os;
}
