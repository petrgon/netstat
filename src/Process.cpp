#include "../include/Process.h"

std::ostream &operator<<(std::ostream &os, const Process &proc)
{
    os << proc.procesId << "/" << proc.processCmdline;
    return os;
}

bool Process::match(Filter *filter)
{
    if (filter == nullptr || filter->regex == "")
        return true;
    return Regex::compare(filter->regex, processCmdline) ||
           Regex::compare(filter->regex, procesId);
}
