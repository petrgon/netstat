
#include <../include/ProcNetParser.h>

const std::string ProcNetParser::NET_TCP_PATH = "/proc/net/tcp";
const std::string ProcNetParser::NET_UDP_PATH = "/proc/net/udp";
const std::string ProcNetParser::NET_TCP6_PATH = "/proc/net/tcp6";
const std::string ProcNetParser::NET_UDP6_PATH = "/proc/net/udp6";

std::list<Socket *> ProcNetParser::parseTcp()
{
    return parse(NET_TCP_PATH, SocketProtocol::tcp);
}

std::list<Socket *> ProcNetParser::parseUdp()
{
    return parse(NET_UDP_PATH, SocketProtocol::udp);
}

std::list<Socket *> ProcNetParser::parseTcp6()
{
    return parse(NET_TCP6_PATH, SocketProtocol::tcp6);
}

std::list<Socket *> ProcNetParser::parseUdp6()
{
    return parse(NET_UDP6_PATH, SocketProtocol::udp6);
}

std::list<Socket *> ProcNetParser::parse(std::string file, SocketProtocol protocol)
{
    std::list<Socket *> socketVector;
    std::ifstream in;
    in.open(file);
    std::string line;
    if (in.is_open())
    {
        std::getline(in, line); // Skip description lines
        while (std::getline(in, line))
        {
            Socket *soc = new Socket();
            std::istringstream iss(line);
            std::string word;
            iss >> word >> soc->localAddress >> soc->remAddress >>
            word >> word >> word >> word >>word >> word >> soc->inode;
            soc->protocol = protocol;
            socketVector.emplace_back(soc);
        }
    }
    in.close();
    return socketVector;
}
