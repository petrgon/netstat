#include "../include/ProcessParser.h"

const std::string ProcessParser::PROCESSES_DIR = "/proc";
const std::string ProcessParser::FILE_DESCRIPTOR_DIR = "fd";
const std::string ProcessParser::CMD_LINE = "cmdline";

std::list<Process *> ProcessParser::getProcessesWithOpenSocket()
{
  std::list<Process *> processes;
  DIR *dir;
  struct dirent *ent;
  if ((dir = opendir(PROCESSES_DIR.c_str())) != NULL)
  {
    while ((ent = readdir(dir)) != NULL)
    {
      if (ent->d_type == DT_DIR && isOnlyNumbers(ent->d_name))
      {
        Process *proc = resolveProcess(PROCESSES_DIR + "/" + ent->d_name);
        if (proc)
        {
          proc->procesId = ent->d_name;
          processes.emplace_back(proc);
        }
      }
    }
  }
  closedir(dir);
  return processes;
}

Process *ProcessParser::resolveProcess(std::string processPath)
{
  std::string processFd = processPath + "/" + FILE_DESCRIPTOR_DIR;
  DIR *dir;
  struct stat statbuf;
  struct dirent *ent;
  Process *proc = nullptr;
  if ((dir = opendir(processFd.c_str())) != NULL)
  {
    while ((ent = readdir(dir)) != NULL)
    {
      std::string entPath = processFd + "/" + ent->d_name;
      if (stat(entPath.c_str(), &statbuf) == -1)
      {
        std::cerr << "Unable read " << entPath << "\n";
        continue;
      }
      if (S_ISSOCK(statbuf.st_mode) || ent->d_type == DT_SOCK)
      {
        if (!proc) // For the first time detected that this process contains any socket
        {
          proc = new Process();
          resolveCmdLine(proc, processPath);
        }
        resolveSocket(proc, entPath);
      }
    }
    closedir(dir);
  }
  return proc;
}

void ProcessParser::resolveCmdLine(Process *proc, std::string procPath)
{
  std::string processPath = procPath + "/" + CMD_LINE;
  std::ifstream in;
  in.open(processPath);
  std::string line;
  if (in.is_open() && std::getline(in, line))
  {
    size_t i = -1, procNameIndex = std::string::npos;
    while(line[++i] != '\0' && line[i] != ' '){
      if(line[i] == '/')
        procNameIndex = i;
    }
    if (procNameIndex != std::string::npos && procNameIndex != line.length() - 1)
    {
      proc->processCmdline = line.substr(procNameIndex + 1);
      in.close();
      return;
    }
    proc->processCmdline = line;
    in.close();
    return;
  }
  proc->processCmdline = "Unable to retrieve process name";
  in.close();
  return;
}

void ProcessParser::resolveSocket(Process *proc, std::string socketPath)
{
  struct stat sb;
  if (lstat(socketPath.c_str(), &sb) == -1)
  {
    perror("lstat");
    exit(EXIT_FAILURE);
  }
  auto linkname = new char[sb.st_size + 1];
  if (linkname == NULL)
  {
    std::cerr << "insufficient memory\n";
    exit(EXIT_FAILURE);
  }
  ssize_t r = readlink(socketPath.c_str(), linkname, sb.st_size + 1);
  if (r < 0)
  {
    perror("lstat");
    exit(EXIT_FAILURE);
  }
  if (r > sb.st_size)
  {
    //"symlink increased in size between lstat() and readlink()\n";
    // skip
    return;
  }
  linkname[r] = '\0';
  std::string linkname_str(linkname);
  delete[] linkname;
  int index = linkname_str.find(":");
  std::string inode_unparsed = linkname_str.substr(index + 1, linkname_str.length());
  std::string inode;
  for (u_int i = 0; i < inode_unparsed.length(); i++)
    if (inode_unparsed[i] >= 48 && inode_unparsed[i] <= 57)
      inode += inode_unparsed[i];
  DataHolder *dh = DataHolder::getInstance();
  auto it = std::find_if(std::begin(dh->tcpSoc), std::end(dh->tcpSoc),
                         [inode](const Socket *soc) { return soc->inode == std::stoi(inode); });
  if (it == std::end(dh->tcpSoc))
    it = std::find_if(std::begin(dh->udpSoc), std::end(dh->udpSoc),
                      [inode](const Socket *soc) { return soc->inode == std::stoi(inode); });
  if (it != std::end(dh->udpSoc))
  {
    proc->sockets.push_back(*it);
    (*it)->process = proc;
  }
}

bool ProcessParser::isOnlyNumbers(std::string name)
{
  for (u_int i = 0; i < name.length(); i++)
  {
    if (name[i] >= 48 && name[i] <= 57)
      continue;
    else if (name[i] == '\0' && i != 0)
      return true;
    else
      return false;
  }
  return true;
}
