#include "../include/Netstat.h"
#include "../include/ArgsResolver.h"

int main(int argc, char **argv)
{
    Netstat netstat;
    ArgsResolver argResolver;
    Filter *filter = argResolver.resolve(argc, argv);
    netstat.printList(filter);
    delete filter;
    return 0;
}