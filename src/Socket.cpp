#include "../include/Socket.h"

std::ostream &operator<<(std::ostream &os, const Socket &soc)
{
    os << std::left << std::setw(6) << soc.protocol
       << std::setw(24) << soc.getLocAdr()
       << std::setw(24) << soc.getRemAdr();
    if (soc.process)
        os << *(soc.process);
    else
        os << "unknown process";
    return os;
}

bool Socket::match(Filter *filter)
{
    if (filter != nullptr)
    {
        if (filter->regex != "")
            return Regex::compare(filter->regex, getLocAdr()) || Regex::compare(filter->regex, getRemAdr());
        else
            return false;
    }
    return true;
}

const std::string Socket::getLocAdr() const
{
    if (cached_locAdr != "")
        return cached_locAdr;
    return IpConvert::hextoip(localAddress);
}
const std::string Socket::getRemAdr() const
{
    if (cached_remAdr != "")
        return cached_remAdr;
    return IpConvert::hextoip(remAddress);
}

const std::string Socket::getLocAdr()
{
    if (cached_locAdr == "")
        cached_locAdr = IpConvert::hextoip(localAddress);
    return cached_locAdr;
}
const std::string Socket::getRemAdr()
{
    if (cached_remAdr == "")
        cached_remAdr = IpConvert::hextoip(remAddress);
    return cached_remAdr;
}
