#include "../include/IpConvert.h"

std::string IpConvert::hextoip(std::string hex)
{
    if (hex.length() < 15)
        return hextoip4(hex);
    else
        return hextoip6(hex);
}

std::string IpConvert::hextoip4(std::string hex)
{
    std::string *hexIp = split(hex);
    std::string output("");
    output = std::to_string(std::stoi(hexIp[0].substr(0, 2), NULL, 16));
    for (int i = 1; i < 4; i++)
    {
        output = std::to_string(std::stoi(hexIp[0].substr(i * 2, 2), NULL, 16)) + "." + output;
    }
    int port = std::stoi(hexIp[1], NULL, 16);
    output += ":" + (port == 0 ? "*" : std::to_string(port));
    delete[] hexIp;
    return output;
}

/*std::string IpConvert::hextoip4(std::string hex)
{
    std::string *hexIp = split(hex);
    sockaddr_in sock;
    std::stringstream ss;
    std::memcpy(&(sock.sin_addr.s_addr), hexIp[0].c_str(), hexIp[0].length());
    sock.sin_family = AF_INET;
    char *c = new char[INET_ADDRSTRLEN];
    inet_ntop(AF_INET, &sock, c, INET_ADDRSTRLEN);
    std::string output(c);
    std::cerr <<hex << " to (" << sock.sin_addr.s_addr <<") " << output << ":" << std::stoi(hexIp[1], NULL, 16) << "\n";
    delete[] hexIp;
    delete[] c;
    return output;
}*/

std::string IpConvert::hextoip6(std::string hex)
{
    std::string *splitted = split(hex);
    std::stringstream ss;
    char *word = new char[5];
    word[4] = '\0';
    bool zerosSkipped = false;
    bool zerosSkipping = false;
    int it = 0;
    for (int i = 0; i < 4; i++)
    {
        it = (8 * (i + 1)) - 1;
        for (int j = 0; j < 4; j++)
        {
            word[(j * 2) % 4] = splitted[0][(it - 1) - (j * 2)];
            word[((j * 2) + 1) % 4] = splitted[0][(it) - (j * 2)];
            if (j % 2 == 1)
            {
                int num = std::stoi(word, NULL, 16);
                if (zerosSkipped || num != 0)
                {
                    ss << std::hex << num;
                    zerosSkipped = zerosSkipping;
                    ss << ":";
                }
                else if (!zerosSkipping)
                {
                    zerosSkipping = true;
                    ss << ":";
                    if (i == 0 && j == 1)
                        ss << ":";
                }
            }
        }
    }
    int port = std::stoi(splitted[1], NULL, 16);
    if (zerosSkipping && !zerosSkipped)
        ss << ":";
    ss << std::dec << (port == 0 ? "*" : std::to_string(port));
    delete[] splitted;
    delete[] word;
    return ss.str();
}

std::string *IpConvert::split(std::string hex)
{
    std::string *arr = new std::string[2];
    int index = hex.find(":");
    arr[0] = hex.substr(0, index);
    arr[1] = hex.substr(index + 1, hex.length());
    return arr;
}