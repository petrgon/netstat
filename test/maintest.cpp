#include <iostream>
#include <list>
#include <cassert>

#include "../include/ProcNetParser.h"
#include "../include/ProcessParser.h"
#include "../include/Socket.h"
#include "../include/IpConvert.h"

void testProcNetParser(){
    ProcNetParser procNetParser;
    auto ret = procNetParser.parseTcp();
    for(Socket * socket : ret){
        std::cout << *socket << "\n";
    }
    //assert(ret.size() == 6 && "TCP Socket Count is not equal 6");
    ret = procNetParser.parseUdp();
    //assert(ret.size() == 2 && "UDP Socket Count is not equal 2");
    for(Socket * socket : ret){
        std::cout << *socket << "\n";
    }
}

void testProcessParser(){
    ProcessParser procParser;
    auto processes = procParser.getProcessesWithOpenSocket();
    for(Process * proc : processes){
        std::cout << *proc << "\n";
    }
}

void testIpConvert(){
    assert(IpConvert::hextoip(std::string("0100007F:0000")).compare("127.0.0.1:*") == 0);
    assert(IpConvert::hextoip(std::string("FE01A8C0:1F90")).compare("192.168.1.254:8080") == 0);
    assert(IpConvert::hextoip(std::string("017AA8C0:0016")).compare("192.168.122.1:22") == 0);
    assert(IpConvert::hextoip(std::string("BACD0120000000000000000052965732:1F90")).compare("2001:cdba::3257:9652:8080") == 0);
    assert(IpConvert::hextoip(std::string("0000000000000000FFFF0000BF00A8C0:0000")).compare("::ffff:192.168.0.191:*") == 0);
}



int main(int argc, char** argv)
{
    //testProcNetParser();
    //testProcessParser();
    testIpConvert();
    return 0;
}